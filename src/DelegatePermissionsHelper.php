<?php

namespace Drupal\delegate_permissions;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\Role;
use Drupal\user\PermissionHandlerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * The DelegatePermissionsHelper service gets roles weights and permissions.
 *
 * @see hook_bypassed_provider_map()
 * @see plugin_api
 */
class DelegatePermissionsHelper {

  /**
   * The "user.permissions" service.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected PermissionHandlerInterface $permissionHandler;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The role storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $roleStorage;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Constructs a DelegatePermissionsHelper object.
   *
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The "user.permissions" service.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(PermissionHandlerInterface $permission_handler, AccountInterface $account, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    $this->permissionHandler = $permission_handler;
    $this->currentUser = $account;
    $this->roleStorage = $entity_type_manager->getStorage('user_role');
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Gets a role weights.
   *
   * @param \Drupal\user\Entity\Role $role
   *   The role.
   *
   * @return int
   *   The role weight.
   */
  protected function getRoleWeight(Role $role): int {

    return $role->getWeight();
  }

  /**
   * Gets all available roles excluding locking roles.
   *
   * @return \Drupal\user\RoleInterface[]
   *   An array of role objects.
   */
  protected function getRoles(): array {
    $roles = $this->roleStorage->loadMultiple();

    return array_diff_key($roles, array_flip(['authenticated', 'anonymous']));
  }

  /**
   * Gets the lower roles for the current user role.
   *
   * @return \Drupal\user\RoleInterface[]
   *   An array of role objects.
   */
  public function getLowerRoles(): array {
    $current_weights = [];
    // Get all roles from current user.
    foreach ($this->currentUser->getRoles(TRUE) as $current_user_role) {

      /** @var \Drupal\user\RoleInterface $current_role */
      $current_role = $this->roleStorage->load($current_user_role);

      $role_id = $current_role->id();
      $current_weights[$role_id] = $this->getRoleWeight($current_role);
    }
    // If current user has administer permission (ie uid 1).
    if ($this->currentUser->hasPermission('administer permissions')) {
      $base_weight = '-9999';
    }
    else {
      // Set the lower role from Current user to use as base.
      $base_weight = $current_weights ? (int) max($current_weights) : '9999';
    }

    $allowed_roles = [];
    // Get all present roles.
    foreach ($this->getRoles() as $role_name => $role) {
      // Set the allowed roles to the ones lower than the base.
      if ($this->getRoleWeight($role) > $base_weight) {
        $allowed_roles[$role_name] = $role;
      }
    }

    return $allowed_roles;
  }

  /**
   * Gets the filtered roles restricted to the current user roles.
   *
   * @return array
   *   An array of permissions.
   */
  public function getRestrictedPerms(): array {
    $permissions = $this->getAllPermissions();
    $config = $this->configFactory->get('delegate_permissions.settings');
    $not_delegable = $config->get('not_delegable');

    $bypassed_providers = $this->calculateBypassedProviders();
    foreach ($permissions as $perm_name => $perm) {
      // Remove if permission is not delegable.
      if (in_array($perm_name, $not_delegable, TRUE)) {
        unset($permissions[$perm_name]);
        continue;
      }
      // If it has a provider bypass permission, skip.
      if (in_array($perm['provider'], $bypassed_providers)) {
        continue;
      }
      if ($this->currentUser->hasPermission($perm_name) === FALSE) {
        unset($permissions[$perm_name]);
      }
    }

    return $permissions;
  }

  /**
   * Gets the array of bypass permissions.
   *
   * @return array
   *   An associative array keyed by provider.
   */
  public function getBypassedProvidersMap(): array {
    $bypassed_providers_map = [
      'node' => 'bypass node access',
      'taxonomy' => 'administer taxonomy',
    ];
    // Allow modules to alter the array map.
    $this->moduleHandler->alter('bypassed_provider_map', $bypassed_providers_map);

    return $bypassed_providers_map;
  }

  /**
   * Gets the list of providers the current user has the permission to bypass.
   *
   * @param \Drupal\user\Entity\Role|null $role
   *   The role to check the permissions.
   *
   * @return array
   *   An array of bypassed providers.
   */
  public function calculateBypassedProviders(?Role $role = NULL): array {
    $bypass_providers_map = $this->getBypassedProvidersMap();
    $bypassed_providers = [];
    foreach ($bypass_providers_map as $provider => $permission) {
      if (($role && $role->hasPermission($permission)) ||
        $this->currentUser->hasPermission($permission)) {
        $bypassed_providers[] = $provider;
      }
    }

    return $bypassed_providers;
  }

  /**
   * Gets all available permissions.
   */
  public function getAllPermissions(): array {

    return $this->permissionHandler->getPermissions();
  }

}

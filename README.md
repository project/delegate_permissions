# Delegate Permissions

## Intro

This contrib module aims to allow "non-admin" Roles (ie: Roles that do not have the `administer permissions`
permission)
to manage and delegate permissions for "lower" Roles.

The role hierarchy (that's why we use the lower/higher terminology) is taken from the Core roles weight configuration.
So higher (more permissive) roles would be able to handle permissions to lower (less permissive) roles.

You might benefit to use this module in projects that have a defined role hierarchy structure, and the need of handling
permissions only by some of those roles. Avoiding the use of the too-broad and sometimes risky `administer permissions`
permission provided by the User Core module.

A good example could be @todo: _insert good example here_ :)

In summary, this module provides a more restricted way to delegate permissions.

**NOTE:** The module restricts the "delegable" permissions to the ones that the current user Role has, in this way the "
higher" role user won't be able to grant permissions that they do not have.

## How to configure it

1. Since the module does some Role weight calculations to check which Roles can handle permissions for other Roles, the
   first thing to do is define the Roles hierarchy on the Role configuration page (`/admin/people/roles`):
   ![](https://i.imgur.com/KNfawMv.png)

2. Grant the provided permission `allow delegate permissions` for the roles that needs to handle permissions on the
   Drupal Permissions page (`/admin/people/permissions`):
   ![](https://i.imgur.com/2ooKsOJ.png)

3. Grant the usual permissions to allow your "higher" Roles to: View the Administration theme, Use the administration
   pages and Access the admin Toolbar (`view the administration theme`,`access administration pages`
   and `access toolbar`)

   This is basically it, but we also provide some more grained configuration:

4. By default, this module **restricts the permissions that can be delegated to the current user permissions**, but it
   also provides a "Not Delegable" configuration (`delegate_permissions.settings`) in case you want to avoid some "tricky"
   permissions to be granted to other roles.

   For example: Your "higher" role needs to be able to create new users, but you do not want that permission to be
   granted to other Roles. In that case, you can set the `administer users` permission to be "not delegable":
   ![](https://i.imgur.com/WXO0iZF.png)
   For this example, the "high" role user would get a form like this on the `/admin/people/delegate-permissions` form:
   ![](https://i.imgur.com/G7X4Xvc.png)

## How to use it

This module provides a form with all permissions that are delegable (that is: All current user permissions, except the
_not delegable_ ones) on `/admin/people/delegate-permissions`, this form can be accessed using the Drupal admin menu
"Configuration" > "People" > "Delegate Permissions"

![](https://i.imgur.com/CPs2sJH.png)

## Config export/import and Delegate Permissions

@todo: _explain how we are dealing with roles config_ :)

## Hooks

This module provides a hook

```php
hook_bypassed_provider_map_alter(&$bypassed_perms) {}
```

That allows other modules to alter the bypassed permissions definitions, by adding items into that associative array (
array map).
See `getBypassedProvidersMap()` method

```php
  /**
   * Gets the array of bypass permissions.
   *
   * @return array
   *   An associative array keyed by provider.
   */
  public function getBypassedProvidersMap() {
    $bypassed_providers_map = [
      'node' => 'bypass node access',
      'taxonomy' => 'administer taxonomy',
    ];
    // Allow modules to alter the array map.
    \Drupal::moduleHandler()->alter('bypassed_provider_map', $bypassed_providers_map);

    return $bypassed_providers_map;
  }
```

If the user has the 'bypass node access' permission, we should offer all "node" provider permissions (CRUD operations on
each Content Type) to be delegable.

Same thing happens with other modules provided permissions that bypass specific checks. So we add those to the array.

You can use this hook like this:

```php
function yourmodule_bypassed_provider_map_alter(&$bypassed_perms) {
  $bypassed_perms += [
    'provider' => 'the broad permission',
  ];
}
```

## Possible improvements
We used the Drupal Roles `weight` to determine role hierarchy to avoid adding yet another configuration form, but we
could:

**a)** Use a configuration form for defining role hierarchy OR

**b)** Restrict in some way the non-secure or non-logical configurations in the Drupal Roles form (ie: setting the Anon
role above the Administrator)
